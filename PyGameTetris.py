import os

import pygame
from pygame.locals import *

from src.statemanager import StateManager
from src.leaderboard import Leaderboard


# game_over_img taken from http://dummyimage.com/
# gfx: kenney - http://kenney.itcxh.io/kenney-donation)
# sfx: sfxr - http://www.drpetter.se/project_sfxr.html
# kenney - http://kenney.itch.io/kenney-donation
def main(window, lb):
    fps = 60  # onlymasterracethings
    os.environ['SDL_VIDEO_CENTERED'] = '1'  # centers the game on the screen
    pygame.display.set_caption("PyGameTetris")
    clock = pygame.time.Clock()
    running = True

    manager = StateManager(lb)

    while running:
        if pygame.event.get(QUIT):
            return

        manager.state.handle_events(pygame.event.get())
        manager.state.update(clock.tick(fps))
        manager.state.render(window)
        pygame.display.flip()


if __name__ == "__main__":
    pygame.mixer.pre_init(44100, -16, 1, 512)  # fixes sound lag, http://stackoverflow.com/a/18513365
    pygame.init()
    screen = pygame.display.set_mode((1280, 768))
    leaderboard = Leaderboard()
    main(screen, leaderboard)
