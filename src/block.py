import random


class Block(object):
    def __init__(self):
        super(Block, self).__init__()

        # red 		= 1
        # green		= 2
        # blue 		= 3
        # yellow 	= 4
        # orange	= 5
        # dark_blue = 6
        # purple	= 7

        I_shapes = [[[3, 3, 3, 3]],
                    [[3],
                     [3],
                     [3],
                     [3]]]

        J_shapes = [[[6, 0, 0],
                     [6, 6, 6]],
                    [[6, 6],
                     [6, 0],
                     [6, 0]],
                    [[6, 6, 6],
                     [0, 0, 6]],
                    [[0, 6],
                     [0, 6],
                     [6, 6]]]

        T_shapes = [[[0, 7, 0],
                     [7, 7, 7]],
                    [[7, 0],
                     [7, 7],
                     [7, 0]],
                    [[7, 7, 7],
                     [0, 7, 0]],
                    [[0, 7],
                     [7, 7],
                     [0, 7]]]

        L_shapes = [[[0, 0, 5],
                     [5, 5, 5]],
                    [[5, 0],
                     [5, 0],
                     [5, 5]],
                    [[5, 5, 5],
                     [5, 0, 0]],
                    [[5, 5],
                     [0, 5],
                     [0, 5]]]

        S_shapes = [[[0, 2, 2],
                     [2, 2, 0]],
                    [[2, 0],
                     [2, 2],
                     [0, 2]]]

        Z_shapes = [[[1, 1, 0],
                     [0, 1, 1]],
                    [[0, 1],
                     [1, 1],
                     [1, 0]]]

        O_shapes = [[[4, 4],
                     [4, 4]]]

        possible_shapes = [I_shapes, J_shapes, L_shapes, O_shapes, S_shapes, T_shapes, Z_shapes]
        next_shape_index = random.randint(0, len(possible_shapes) - 1)

        self.shapes = possible_shapes[next_shape_index]
        self.shape_cur = 0
        self.potential_shape_cur = 0
        self.shape = self.shapes[self.shape_cur]
        self.potential_shape = [0]
        self.top_left = [-2, 4]
        self.potential_top_left = [0, 0]
