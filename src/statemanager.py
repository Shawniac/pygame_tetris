from src.mainmenustate import MainMenuState
from src.playstate import PlayState


class StateManager(object):
    def __init__(self, leaderboard):
        self.state = None
        self.leaderboard = leaderboard
        self.go_to("main_menu")

    def go_to(self, state):
        if state == "play":
            self.state = PlayState(self.leaderboard)
        elif state == "main_menu":
            self.state = MainMenuState(self.leaderboard)

        self.state.manager = self
# TODO: implement a better state manager