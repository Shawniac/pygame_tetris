import os

import pygame
from pygame.locals import *

import src.utilities


class Button(object):
    def __init__(self, type):
        super(Button, self).__init__()
        self.hover = False
        self.down = False
        self.clicked = False
        self.type = type

        if self.type == "play":
            self.imageDefault = pygame.image.load(os.path.join("assets", "gfx", "play_default.png"))
            self.imageHover = pygame.image.load(os.path.join("assets", "gfx", "play_hover.png"))
            self.imageDown = pygame.image.load(os.path.join("assets", "gfx", "play_down.png"))

            self.image = self.imageDefault
            self.width, self.height = self.image.get_size()
            self.x = src.utilities.centered(self.image)[0]
            self.y = 400
        elif self.type == "exit":
            self.imageDefault = pygame.image.load(os.path.join("assets", "gfx", "exit_default.png"))
            self.imageHover = pygame.image.load(os.path.join("assets", "gfx", "exit_hover.png"))
            self.imageDown = pygame.image.load(os.path.join("assets", "gfx", "exit_down.png"))

            self.image = self.imageDefault
            self.width, self.height = self.image.get_size()
            self.x = src.utilities.centered(self.image)[0]
            self.y = 450

    def handle_events(self, events):
        for event in events:
            if event.type == MOUSEMOTION:
                mouse_x, mouse_y = event.pos

                if (not self.hover) and (mouse_x > self.x and mouse_x < self.x + self.width
                                         and mouse_y > self.y and mouse_y < self.y + self.height):
                    self.hover = True
                elif self.hover and not (mouse_x > self.x and mouse_x < self.x + self.width
                                         and mouse_y > self.y and mouse_y < self.y + self.height):
                    self.hover = False
            if event.type == MOUSEBUTTONDOWN:
                mouse_x, mouse_y = event.pos

                if (not self.down) and (mouse_x > self.x and mouse_x < self.x + self.width
                                        and mouse_y > self.y and mouse_y < self.y + self.height):
                    if event.button == 1:
                        self.y += 4
                        self.down = True
            if event.type == MOUSEBUTTONUP:
                if self.down:
                    self.y -= 4
                    self.down = False
                    self.clicked = True
