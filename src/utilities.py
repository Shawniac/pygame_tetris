import pygame


def centered(surface):
    screen_width, screen_height = pygame.display.get_surface().get_size()
    return (screen_width - surface.get_width()) / 2, (screen_height - surface.get_height()) / 2
