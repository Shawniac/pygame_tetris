import os

import pygame
from pygame.locals import *

import psutil

from src.gamestate import GameState
import src.utilities
from src.block import Block


# collision handling and basic setup:
# http://gamedevelopment.tutsplus.com/tutorials/implementing-tetris-collision-detection--gamedev-852
class PlayState(GameState):
    def __init__(self, leaderboard):
        super(GameState, self).__init__()
        # constants
        self.DEBUG_STATE = False
        self.BLOCK_SPEED = 250.0  # fall every 250ms
        self.SPEEDUP = 1000  # speedup every second
        self.ROW_SCORE = 100
        self.FIELD_OFFSET_X = 15 * 32
        self.FIELD_OFFSET_Y = 2 * 32
        self.FIELD_WIDTH = 10
        self.FIELD_HEIGHT = 20
        self.PREVIEW_OFFSET_X = 29 * 32
        self.PREVIEW_OFFSET_Y = 3 * 32
        # variables
        self.cpu = psutil.cpu_percent()
        self.mem = psutil.virtual_memory().percent
        self.time_since_block_moved = 0
        self.time_since_speedup = 0
        self.rows_completed = 0
        self.rows_completed_total = 0
        self.game_over = False
        self.msgText = "PyGameTetris"
        self.username = "NoName"
        self.leaderboard = leaderboard
        self.score_cur = 0
        # fonts
        self.font = pygame.font.Font(
            os.path.join("assets", "gfx", "fonts", "BebasNeue.otf"), 32
        )
        # images
        self.images = {}
        image_names = ["background", "game_over"]
        for image_name in image_names:
            self.images[image_name] = pygame.image.load(
                os.path.join("assets", "gfx", image_name + ".png")
            ).convert()
        block_image_names = [
            "red",
            "green",
            "blue",
            "yellow",
            "orange",
            "dark_blue",
            "purple",
        ]
        for index in range(len(block_image_names)):
            self.images[index + 1] = pygame.image.load(
                os.path.join(
                    "assets",
                    "gfx",
                    "element_" + block_image_names[index] + "_square.png",
                )
            ).convert()
        # sounds
        self.drop_block_sound = pygame.mixer.Sound(
            os.path.join("assets", "sfx", "drop_block.wav")
        )
        self.line_removed_sound = pygame.mixer.Sound(
            os.path.join("assets", "sfx", "line_removed.wav")
        )
        # containers
        # Text displays the game speed, current score, and total lines completed
        self.game_stats = [
            "Speed: {0:.2f}".format(1000 // self.BLOCK_SPEED),
            "Score: {0}".format(self.score_cur),
            "Lines: {0}".format(self.rows_completed_total),
        ]
        self.debug = ["CPU: %d" % self.cpu, "MEM: %d" % self.mem]
        self.block_cur = Block()
        self.block_next = Block()
        self.landed = []
        for row in range(self.FIELD_HEIGHT):
            self.landed.append([0] * self.FIELD_WIDTH)

    def render(self, screen):
        screen.fill((200, 200, 200))
        # draw the game background
        screen.blit(self.images["background"], (0, 0))
        # draw the falling block
        if self.block_cur is not None:
            for row in range(len(self.block_cur.shape)):
                for col in range(len(self.block_cur.shape[row])):
                    if self.block_cur.top_left[0] + row >= 0:
                        image = self.images.get(self.block_cur.shape[row][col])
                        if image is not None:
                            block_width, block_height = image.get_size()
                            screen.blit(
                                image,
                                (
                                    self.FIELD_OFFSET_X
                                    + (col + self.block_cur.top_left[1]) * block_width,
                                    self.FIELD_OFFSET_Y
                                    + (row + self.block_cur.top_left[0]) * block_height,
                                ),
                            )
        # draw the landed blocks
        for row in range(len(self.landed)):
            for col in range(len(self.landed[row])):
                image = self.images.get(self.landed[row][col])
                if image is not None:
                    block_width, block_height = image.get_size()
                    screen.blit(
                        image,
                        (
                            self.FIELD_OFFSET_X + col * block_width,
                            self.FIELD_OFFSET_Y + row * block_height,
                        ),
                    )
        # draw the next block
        if self.block_next is not None:
            for row in range(len(self.block_next.shape)):
                for col in range(len(self.block_next.shape[row])):
                    image = self.images.get(self.block_next.shape[row][col])
                    if image is not None:
                        block_width, block_height = image.get_size()
                        screen.blit(
                            image,
                            (
                                self.PREVIEW_OFFSET_X + col * block_width,
                                self.PREVIEW_OFFSET_Y + row * block_height,
                            ),
                        )
        # draw debug text
        if self.DEBUG_STATE:
            for i in range(len(self.debug)):
                debug_msg = self.font.render(self.debug[i], True, (0, 0, 0))
                screen.blit(debug_msg, (1 * 32, 1 * 32 + i * 32))
        # draw other text
        for i in range(len(self.game_stats)):
            text_msg = self.font.render(self.game_stats[i], True, (0, 0, 0))
            screen.blit(text_msg, (5 + 29 * 32, 8 * 32 + i * 32))
        # draw the game-over "screen"
        if self.game_over:
            screen.blit(
                self.images["game_over"],
                src.utilities.centered(self.images["game_over"]),
            )
            username = self.font.render(self.username, True, (255, 255, 255))
            screen.blit(
                username,
                (
                    src.utilities.centered(username)[0],
                    src.utilities.centered(username)[1] + 60,
                ),
            )

    # TODO: cleanup and extract update loop
    def update(self, dt):
        if not self.game_over and self.block_cur is not None:
            self.time_since_block_moved += dt

            if self.time_since_block_moved >= self.BLOCK_SPEED:
                self.time_since_speedup += self.time_since_block_moved

                if self.time_since_speedup >= self.SPEEDUP:
                    self.BLOCK_SPEED *= 0.99
                    self.time_since_speedup = 0

                if (
                    0
                    <= self.block_cur.top_left[0]
                    < len(self.landed) - len(self.block_cur.shape)
                ):
                    self.block_cur.potential_top_left[0] = (
                        self.block_cur.top_left[0] + 1
                    )
                    self.block_cur.potential_top_left[1] = self.block_cur.top_left[1]

                    if not self.is_valid_move():
                        self.end_block()
                        return

                    self.block_cur.top_left[0] = self.block_cur.potential_top_left[0]
                elif self.block_cur.top_left[0] < 0:
                    self.block_cur.potential_top_left[0] = (
                        self.block_cur.top_left[0] + 1
                    )
                    self.block_cur.potential_top_left[1] = self.block_cur.top_left[1]

                    if not self.is_valid_move():
                        self.game_over = True
                    else:
                        self.block_cur.top_left[0] = self.block_cur.potential_top_left[
                            0
                        ]
                elif self.block_cur.top_left[0] == len(self.landed) - len(
                    self.block_cur.shape
                ):
                    self.end_block()
                self.time_since_block_moved = 0

                self.game_stats = [
                    "Speed: {0:.2f}".format(1000 // self.BLOCK_SPEED),
                    "Score: {0}".format(self.score_cur),
                    "Lines: {0}".format(self.rows_completed_total),
                ]

                self.cpu = psutil.cpu_percent()
                self.mem = psutil.virtual_memory().percent
                self.debug = ["CPU: %d" % self.cpu, "MEM: %d" % self.mem]

    # TODO: clarify if logic in this method is suitable [edit: seems to be better to use pygame.events.get()]
    def handle_events(self, events):
        for event in events:
            # if event.type == MOUSEMOTION:
            # self.mouse_x, self.mouse_y = event.pos
            # if event.type == MOUSEBUTTONUP:
            # self.mouse_x, self.mouse_y = event.pos

            # if event.button in (1,2,3):
            # self.msgText = 'left, middle, or right mouse clicked'
            # elif event.button in (4, 5):
            # self.msgText = 'mouse scrolled up or down'

            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    pygame.event.post(pygame.event.Event(QUIT))
                elif event.key == K_d:
                    self.DEBUG_STATE = not self.DEBUG_STATE

                if self.block_cur is not None and not self.game_over:
                    if event.key == K_UP:
                        self.block_cur.potential_shape_cur = (
                            self.block_cur.shape_cur + 1
                        ) % len(self.block_cur.shapes)
                        self.block_cur.potential_shape = self.block_cur.shapes[
                            self.block_cur.potential_shape_cur
                        ]

                        if not self.is_valid_rotation():
                            return
                        else:
                            self.block_cur.shape_cur = (
                                self.block_cur.potential_shape_cur
                            )
                            self.block_cur.shape = self.block_cur.potential_shape
                    if event.key == K_DOWN:
                        self.drop_block_sound.play()

                        self.block_cur.potential_top_left[0] = (
                            self.block_cur.top_left[0] + 1
                        )
                        self.block_cur.potential_top_left[1] = self.block_cur.top_left[
                            1
                        ]
                        while self.is_valid_move():
                            self.block_cur.top_left[0] = (
                                self.block_cur.potential_top_left[0]
                            )

                            self.block_cur.potential_top_left[0] = (
                                self.block_cur.top_left[0] + 1
                            )
                            self.block_cur.potential_top_left[1] = (
                                self.block_cur.top_left[1]
                            )

                        self.end_block()
                    if event.key == K_LEFT:
                        if self.block_cur.top_left[1] > 0:
                            self.block_cur.potential_top_left[0] = (
                                self.block_cur.top_left[0]
                            )
                            self.block_cur.potential_top_left[1] = (
                                self.block_cur.top_left[1] - 1
                            )

                            if not self.is_valid_move():
                                return
                            else:
                                self.block_cur.top_left[1] = (
                                    self.block_cur.potential_top_left[1]
                                )
                    if event.key == K_RIGHT:
                        if self.block_cur.top_left[1] + len(
                            self.block_cur.shape[0]
                        ) < len(self.landed[0]):
                            self.block_cur.potential_top_left[0] = (
                                self.block_cur.top_left[0]
                            )
                            self.block_cur.potential_top_left[1] = (
                                self.block_cur.top_left[1] + 1
                            )

                            if not self.is_valid_move():
                                return
                            else:
                                self.block_cur.top_left[1] = (
                                    self.block_cur.potential_top_left[1]
                                )
                elif self.game_over:
                    if event.type == pygame.KEYDOWN:
                        if event.unicode == "\b":
                            self.username = self.username[:-1]
                        elif event.unicode == "\r":
                            self.leaderboard.save_scores(self.username, self.score_cur)
                            self.manager.go_to("main_menu")
                        elif len(self.username) < 16:
                            self.username += event.unicode

    def is_valid_move(self):
        for row in range(len(self.block_cur.shape)):
            for col in range(len(self.block_cur.shape[row])):
                if self.block_cur.shape[row][col] != 0:
                    if (
                        row + self.block_cur.potential_top_left[0] >= 0
                        and col + self.block_cur.potential_top_left[1] >= 0
                    ):
                        if (
                            self.block_cur.potential_top_left[0] + row
                            >= len(self.landed)
                            or self.landed[row + self.block_cur.potential_top_left[0]][
                                col + self.block_cur.potential_top_left[1]
                            ]
                            != 0
                        ):
                            return False
        return True

    def is_valid_rotation(self):
        for row in range(len(self.block_cur.potential_shape)):
            for col in range(len(self.block_cur.potential_shape[row])):
                if self.block_cur.potential_shape[row][col] != 0:
                    if (
                        self.block_cur.top_left[1] + col >= len(self.landed[0])
                        or self.block_cur.top_left[0] + row >= len(self.landed)
                        or self.landed[row + self.block_cur.top_left[0]][
                            col + self.block_cur.top_left[1]
                        ]
                        != 0
                    ):
                        return False
        return True

    def land_block(self):
        for row in range(len(self.block_cur.shape)):
            for col in range(len(self.block_cur.shape[row])):
                if self.block_cur.shape[row][col] != 0:
                    self.landed[row + self.block_cur.top_left[0]][
                        col + self.block_cur.top_left[1]
                    ] = self.block_cur.shape[row][col]

    def remove_full_lines(self):
        for row in range(len(self.landed)):
            if 0 not in self.landed[row]:
                self.landed[row] = [0] * 10
                self.rows_completed += 1
                # apply tetris gravity
                if row - 1 > 0:
                    for temp_row in range(row - 1, 0, -1):
                        self.landed[temp_row + 1] = self.landed[temp_row]
                        self.landed[temp_row] = [0] * 10

    def update_score(self):
        if self.rows_completed > 0:
            self.line_removed_sound.play()
            self.score_cur += self.rows_completed**2 * self.ROW_SCORE
            print(self.score_cur)
            self.rows_completed_total += self.rows_completed
            print(self.rows_completed_total)
            self.rows_completed = 0

    def end_block(self):
        self.land_block()
        self.remove_full_lines()
        self.update_score()
        # spawn a new block at the top
        self.block_cur = self.block_next
        self.block_next = Block()
