import pickle


class Leaderboard(object):
    def __init__(self):
        super(Leaderboard, self).__init__()
        self.scores = self.load_scores()

    # get the top n-scores
    def get_top_scores(self, n):
        return sorted(self.scores.items(), key=lambda x: -x[1])[:n]

    def save_scores(self, name, score):
        self.scores[name] = score

        with open('scores.dat', 'wb') as scores_file:
            pickle.dump(self.scores, scores_file)

    def load_scores(self):
        try:
            with open('scores.dat', 'rb') as scores_file:
                return pickle.load(scores_file)
        except:
            return {}
