import os

import pygame
from pygame.locals import *

from src.gamestate import GameState
from src.button import Button
import src.utilities
from src.block import Block


class MainMenuState(GameState):
    def __init__(self, leaderboard):
        super(MainMenuState, self).__init__()
        self.font = pygame.font.Font(os.path.join('assets', 'gfx', 'fonts', 'BebasNeue.otf'), 64)
        self.font2 = pygame.font.Font(os.path.join('assets', 'gfx', 'fonts', 'BebasNeue.otf'), 32)
        self.font3 = pygame.font.Font(os.path.join('assets', 'gfx', 'fonts', 'BebasNeue.otf'), 24)
        self.buttons = [Button("play"), Button("exit")]
        self.scores_top = leaderboard.get_top_scores(3)
        self.blocks = [Block(), Block(), Block(), Block(), Block(), Block()]
        for i in range(len(self.blocks)):
            if i > 0:
                self.blocks[i].top_left[1] = self.blocks[i - 1].top_left[1] + len(self.blocks[i - 1].shape[0]) + 1
        self.images = {}
        block_image_names = ['red', 'green', 'blue', 'yellow', 'orange', 'dark_blue', 'purple']
        for index in range(len(block_image_names)):
            self.images[index + 1] = pygame.image.load(
                os.path.join('assets', 'gfx', 'element_' + block_image_names[index] + '_square.png')).convert()
        self.time_since_block_moved = 0

    def render(self, screen):
        screen.fill((0, 200, 0))

        for block in self.blocks:
            for row in range(len(block.shape)):
                for col in range(len(block.shape[row])):
                    image = self.images.get(block.shape[row][col])
                    if image is not None:
                        block_width, block_height = image.get_size()
                        screen.blit(image,
                                    ((col + block.top_left[1]) * block_width, (row + block.top_left[0]) * block_height))

        text = self.font.render("PyGameTetris", True, (255, 255, 255))
        screen.blit(text, (src.utilities.centered(text)[0], 200))

        score_msg = self.font2.render("Score: ", True, (255, 255, 255))
        screen.blit(score_msg, (10, 10))

        for score in range(len(self.scores_top)):
            score_msg = self.font3.render(str(self.scores_top[score][0]) + ": " + str(self.scores_top[score][1]), True,
                                          (255, 255, 255))
            screen.blit(score_msg, (10, 50 + 25 * score))

        for button in self.buttons:
            screen.blit(button.image, (button.x, button.y))

    def update(self, dt):
        self.time_since_block_moved += dt

        if self.time_since_block_moved >= 250:
            for block in self.blocks:
                block.top_left[0] += 1
                self.time_since_block_moved = 0

        for button in self.buttons:
            if button.type == "play" and button.clicked:
                self.manager.go_to("play")
                button.clicked = False
            elif button.type == "exit" and button.clicked:
                pygame.event.post(pygame.event.Event(QUIT))
            elif button.down:
                button.image = button.imageDown
            elif button.hover:
                button.image = button.imageHover
            else:
                button.image = button.imageDefault

    def handle_events(self, events):
        for event in events:
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    pygame.event.post(pygame.event.Event(QUIT))
                elif event.key == K_RETURN:
                    self.manager.go_to("play")

        for button in self.buttons:
            button.handle_events(events)
