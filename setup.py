from cx_Freeze import setup, Executable

includes = ['re', 'pygame']
excludes = []
packages = []
path = []

exe = Executable(
    script="pygametetris",
    initScript=None,
    base="Win32GUI",
    targetDir="dist",
    targetName="pygametetris.exe",
    compress=True,
    copyDependentFiles=True,
    appendScriptToExe=False,
    appendScriptToLibrary=False,
    icon=None
)

setup(
    version="0.3",
    description="A Tetris implentation using PyGame",
    author="Shawniac",
    name="PyGameTetris",

    options={
        "build_exe": {
            "includes": includes,
            "excludes": excludes,
            "packages": packages,
            "path": path
        }
    },
    executables=[exe], requires=['pygame', 'psutil', 'cx_Freeze']
)
